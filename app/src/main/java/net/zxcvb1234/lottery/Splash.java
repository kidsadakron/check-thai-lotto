package net.zxcvb1234.lottery;

/**
 * Created by kidsadakron on 5/3/16.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import gcm.play.android.samples.com.gcmquickstart.R;

public class Splash extends Activity {
    Handler handler;
    Runnable runnable;
    long delay_time;
    long time = 3000L;
    private static final int SPLASH_TIME = 3000;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splashscreen);
        startHeavyProcessing();
    }
    private void startHeavyProcessing(){
        new BackgroundTask().execute("");
    }


    private class BackgroundTask extends AsyncTask<String, Void, String> {
        Intent intent;

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL("http://www.zxcvb1234.net/lotto/test.php");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConnection.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            intent = new Intent(Splash.this, MainActivity.class);

        }



        @Override
        protected void onPostExecute(String result) {
//            super.onPostExecute(result);

//            Pass your loaded data here using Intent

            intent.putExtra("prize", result);
            startActivity(intent);
            finish();
        }
    }
}
