package net.zxcvb1234.lottery;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import gcm.play.android.samples.com.gcmquickstart.R;

/**
 * Created by kidsadakron on 5/4/16.
 */
public class Tab1 extends Fragment {
    private TextView tvDate,tvP32,tvP31,tvP2,tvP1;
    Button btToCheckList;
    ShareDialog shareDialog;
    ImageButton ibFbshare;
    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         v =inflater.inflate(R.layout.tab_1,container,false);
        tvDate = (TextView) v.findViewById(R.id.tvDate);
        tvP1 = (TextView) v.findViewById(R.id.tvP1);
        tvP2 = (TextView) v.findViewById(R.id.tvP2);
        tvP31 = (TextView) v.findViewById(R.id.tvP31);
        tvP32= (TextView) v.findViewById(R.id.tvP32);
        btToCheckList = (Button) v.findViewById(R.id.btOther);
        ibFbshare = (ImageButton) v.findViewById(R.id.ibFbshare);
        Bundle args = getActivity().getIntent().getExtras();
        String prize= args.getString("prize");
        JSONObject mJsonArray = null;
        FacebookSdk.sdkInitialize(getContext());
        shareDialog = new ShareDialog(getActivity());
        try {
            mJsonArray = new JSONObject(prize);
            String date =  mJsonArray.getString("date");
            String prize_1 =  mJsonArray.getString("prize_1");
            String prize_2 =  mJsonArray.getString("prize_2");
            String prize_31f =  mJsonArray.getString("prize_31f");
            String prize_32f =  mJsonArray.getString("prize_32f");
            tvDate.setText(date);
            tvP1.setText(prize_1);
            tvP2.setText(prize_2);
            tvP31.setText(prize_31f);
            tvP32.setText(prize_32f);

            Log.d("data", prize);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ibFbshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap mbitmap = getBitmapOFRootView(v);
//                Bitmap mbitmap = BitmapFactory.decodeResource(getResources(), R.drawable.logo_app);
                SharePhoto photo = new SharePhoto.Builder()
                        .setBitmap(mbitmap)
                        .build();
                SharePhotoContent content = new SharePhotoContent.Builder()
                        .addPhoto(photo)
                        .build();
                shareDialog.show(content);
//                try {
//                    URL url = new URL("http://www.google.co.th");
//
//                ShareLinkContent linkContent = new ShareLinkContent.Builder()
//                        .setImageUrl(Uri.parse("http://www.google.co.th")).build();
//                shareDialog.show(linkContent);
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                }
            }
        });
        btToCheckList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Intent intent = new Intent(getActivity(),CheckListActivity.class);
                //startActivity(intent);
                String url = "http://www.glo.or.th/main.php?filename=glo_lotto";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    }

    public Bitmap getBitmapOFRootView(View v) {
        View rootview = v.getRootView();
        rootview.setDrawingCacheEnabled(true);
        Bitmap bitmap1 = rootview.getDrawingCache();
        return bitmap1;
    }

}