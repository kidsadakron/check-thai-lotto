package net.zxcvb1234.lottery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import gcm.play.android.samples.com.gcmquickstart.R;

/**
 * Created by kidsadakron on 5/21/16.
 */
public class CustomListViewHistoryAdapter extends BaseAdapter {
    Context mContext;
    String[] strName;
    String[] strStatus;
    public CustomListViewHistoryAdapter(Context context, String[] strName,String[] strStatus) {
        this.mContext= context;
        this.strName = strName;
        this.strStatus = strStatus;
    }
    @Override
    public int getCount() {
        return strName.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater =
                (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView == null)
            convertView = mInflater.inflate(R.layout.listview_row, parent, false);

        TextView textView = (TextView)convertView.findViewById(R.id.textView1);
        TextView tvLottoStatus = (TextView) convertView.findViewById(R.id.tvLottoStatus);
        textView.setText(strName[position]);
        tvLottoStatus.setText(strStatus[position]);



        return convertView;
    }
}
