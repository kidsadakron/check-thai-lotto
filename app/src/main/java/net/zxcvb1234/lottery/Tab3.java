package net.zxcvb1234.lottery;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

import gcm.play.android.samples.com.gcmquickstart.R;

/**
 * Created by kidsadakron on 5/4/16.
 */
public class Tab3 extends Fragment {
String facebook_id;
    ListView listView;
    TextView tvSum;
    int w,y,n = 0 ;
    public Tab3(){
        Log.d("testFrag","ok");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.tab_3, container, false);



         listView = (ListView)v.findViewById(R.id.listView1);
        tvSum = (TextView)v.findViewById(R.id.tvSum);


        return v;
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        GraphRequestAsyncTask request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject user, GraphResponse graphResponse) {

                Log.d("ss", user.optString("id"));
                facebook_id = user.optString("id");
                new BackgroundTask().execute("");
            }
        }).executeAsync();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private class BackgroundTask extends AsyncTask<String, Void, String> {
        Intent intent;

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL("http://www.zxcvb1234.net/lotto/getLotto.php?facebook_id="+facebook_id);
                Log.d("url",url.toString());
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConnection.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            intent = new Intent(Splash.this, MainActivity.class);

        }



        @Override
        protected void onPostExecute(String result) {


            try {
                JSONArray js = new JSONArray(result);
                Log.d("dataLotto",result);
                String[] list = new String[js.length()];
                String[] listStatus = new String[js.length()];
                for(int i = 0 ; i< js.length() ; i++){
                    JSONObject j = js.getJSONObject(i);
                    String[] temp = j.getString("lotto_date").split(" ");
                    list[i] = "วันที่ : "+temp[0]+" หมายเลข : "+j.getString("lotto_buy");
                    if (j.getString("lotto_status").equals("0")) {
                        listStatus[i] = "รอผล";
                        w++;
                    }else if (j.getString("lotto_status").equals("1")) {
                        listStatus[i] = "ไม่ถูก";
                        n++;
                    }else if (j.getString("lotto_status").equals("2")) {
                        listStatus[i] = "ถูก";
                        y++;
                    }

                }
                float fy = (float) y;
                float fn = (float) n;

                float persent = (fy/(fn+fy));
              //  Log.d("y" , ""+(y+n));
                //Log.d("persent" , ""+(y/3.0));
                DecimalFormat df = new DecimalFormat();
                df.setMaximumFractionDigits(2);

                //persent = df.format(persent);
if(fy != 0 && fn !=0){
    tvSum.setText("ถูก : " + df.format(persent * 100) + "% , ไม่ถูก : " + df.format(100 - (persent * 100))+"%");
}

                CustomListViewHistoryAdapter adapter = new CustomListViewHistoryAdapter(getActivity(), list,listStatus);
                listView.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }catch (Exception e){

            }
//            super.onPostExecute(result);

//            Pass your loaded data here using Intent

//            intent.putExtra("prize", result);
//            startActivity(intent);
//            finish();
        }
    }
}