package net.zxcvb1234.lottery;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import gcm.play.android.samples.com.gcmquickstart.R;

/**
 * Created by kidsadakron on 5/4/16.
 */
public class Tab2 extends Fragment {
//    public ImageButton ibToCamera;
    public EditText edInputLotto;
    public Button btSave ,ibToCamera;
    CharSequence Titles[]={"หน้าแรก","บันทึกสลาก","ประวัติ"};
    int Numboftabs =3;
//    ViewPager pager;
    CustomListViewHistoryAdapter ca;
    String facebook_id,textInput;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.tab_2,container,false);

        ibToCamera = (Button) v.findViewById(R.id.toCamera);
        edInputLotto = (EditText) v.findViewById(R.id.edInputLotto);
        btSave = (Button) v.findViewById(R.id.btSave);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textInput = edInputLotto.getText().toString();
                new BackgroundTask().execute("");
            }
        });
        ibToCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),EnterImageActivity.class);

               startActivityForResult(intent,101);
            }
        });

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        GraphRequestAsyncTask request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject user, GraphResponse graphResponse) {

                Log.d("ss", user.optString("id"));
                facebook_id = user.optString("id");

            }
        }).executeAsync();


        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101){
            if(resultCode == getActivity().RESULT_OK){
                edInputLotto.setText(data.getStringExtra("text"));
            }
        }
    }

    private class BackgroundTask extends AsyncTask<String, Void, String> {
        Intent intent;

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            StringBuilder result = new StringBuilder();


//            URL url = new URL("http://192.168.1.3:8888/user.php");
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://www.zxcvb1234.net/lotto/user.php");

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("facebook_id", facebook_id));
                nameValuePairs.add(new BasicNameValuePair("lotto_buy", textInput));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                Log.d("facebook_id",facebook_id);

            } catch (ClientProtocolException e) {
                Log.e("error",e.toString());
                // TODO Auto-generated catch block
            } catch (IOException e) {
                Log.e("error",e.toString());
                // TODO Auto-generated catch block
            }


            return result.toString();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();




        }



        @Override
        protected void onPostExecute(String result) {
//            AlertDialog dg  = new AlertDialog.Builder(getActivity()).create();
//            dg.setTitle("test");
//            dg.setMessage("Message");
//            dg.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
////                    pager.setCurrentItem(2);
//                    dialog.dismiss();
//                }
//            });
//            dg.show();

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("ผลการบันทึก");
            builder.setMessage("บันทึกเรียบร้อนแล้ว");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    edInputLotto.setText("");
                  ((MainActivity2)getActivity()).getView();
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.show();
        }
    }
}