package net.zxcvb1234.lottery;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import gcm.play.android.samples.com.gcmquickstart.R;

/**
 * Created by kidsadakron on 5/21/16.
 */
public class CheckListActivity extends AppCompatActivity {
//    public ImageButton ibToCamera;
    public EditText edInputLotto;
    public Button btSave,ibToCamera;
    String inputLotto;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_list);
        ibToCamera = (Button) findViewById(R.id.toCamera);
        edInputLotto = (EditText) findViewById(R.id.edInputLotto);
        btSave = (Button) findViewById(R.id.btSave);

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputLotto  =  edInputLotto.getText().toString();
                new BackgroundTask().execute("");

            }
        });
        ibToCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckListActivity.this,EnterImageActivity.class);

                startActivityForResult(intent,101);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101){
            if(resultCode == CheckListActivity.RESULT_OK){
                edInputLotto.setText(data.getStringExtra("text"));
            }
        }
    }


    private class BackgroundTask extends AsyncTask<String, Void, String> {
        Intent intent;

        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection urlConnection = null;
            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL("http://www.zxcvb1234.net/lotto/check.php?lnumber="+inputLotto);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConnection.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();




        }



        @Override
        protected void onPostExecute(String result) {
            Log.d("xx",result);

            AlertDialog.Builder builder = new AlertDialog.Builder(CheckListActivity.this);
            builder.setTitle("ผลการตรวจ");
            builder.setMessage(result);
            builder.setPositiveButton("OK", null);
            AlertDialog dialog = builder.show();
//
//            AlertDialog dg  = new AlertDialog.Builder(CheckListActivity.this).create();
//            dg.setTitle("test");
//            dg.setMessage(result);
//            dg.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
////                    pager.setCurrentItem(2);
//                    dialog.dismiss();
//                }
//            });
            // Must call show() prior to fetching text view
            TextView messageView = (TextView)dialog.findViewById(android.R.id.message);
            messageView.setGravity(Gravity.CENTER);
            messageView.setTextSize(20.0f);
//            dg.show();
        }

    }
}
