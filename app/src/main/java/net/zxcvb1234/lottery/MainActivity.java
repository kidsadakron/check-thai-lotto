/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.zxcvb1234.lottery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import gcm.play.android.samples.com.gcmquickstart.R;

public class MainActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private TextView tvDate,tvP32,tvP31,tvP2,tvP1;
    private boolean isReceiverRegistered;
    Button btCheck;
    CallbackManager callbackManager;
    LoginButton login;
    String prize;
    AccessTokenTracker accessTokenTracker;
    String sign;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

//        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        Intent intentPrize =  getIntent();
         prize = intentPrize.getStringExtra("prize");
        JSONObject mJsonArray = null;
        tvDate = (TextView) findViewById(R.id.tvDate);
        btCheck = (Button) findViewById(R.id.btOther);
        tvP1 = (TextView) findViewById(R.id.tvP1);
        tvP2 = (TextView) findViewById(R.id.tvP2);
        tvP31 = (TextView) findViewById(R.id.tvP31);
        tvP32= (TextView) findViewById(R.id.tvP32);
        login = (LoginButton)findViewById(R.id.login_button);
        login.setReadPermissions("email");
        getLoginDetails(login);
        btCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Intent intent = new Intent(MainActivity.this, CheckListActivity.class);
                //startActivity(intent);

                String url = "http://www.glo.or.th/main.php?filename=glo_lotto";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);


            }
        });

        try {
            PackageInfo info = getPackageManager().getPackageInfo("net.zxcvb1234.lottery",     PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                 sign= Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);
                //Toast.makeText(getApplicationContext(), sign, Toast.LENGTH_LONG).show();
            }

        } catch (NoSuchAlgorithmException e) {
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        try {
            mJsonArray = new JSONObject(prize);
          String date =  mJsonArray.getString("date");
            String prize_1 =  mJsonArray.getString("prize_1");
            String prize_2 =  mJsonArray.getString("prize_2");
            String prize_31f =  mJsonArray.getString("prize_31f");
            String prize_32f =  mJsonArray.getString("prize_32f");
            tvDate.setText(date);
            tvP1.setText(prize_1);
            tvP2.setText(prize_2);
            tvP31.setText(prize_31f);
            tvP32.setText(prize_32f);

            Log.d("data",prize);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
//                    mInformationTextView.setText(getString(R.string.gcm_send_message));
                } else {
//                    mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };
//        mInformationTextView = (TextView) findViewById(R.id.informationTextView);


        // Registering BroadcastReceiver
        registerReceiver();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
        AccessToken token = AccessToken.getCurrentAccessToken();
        if (token != null) {
            Intent intent = new Intent(MainActivity.this,MainActivity2.class);
            intent.putExtra("prize",prize);
            startActivity(intent);
            //Toast.makeText(MainActivity.this,"ssss",Toast.LENGTH_LONG).show();
        }


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.e("data",data.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;

        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    protected void getLoginDetails(LoginButton login_button){

        // Callback registration
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult login_result) {





                new BackgroundTask().execute("");

            }

            @Override
            public void onCancel() {
                // code for cancellation
            }

            @Override
            public void onError(FacebookException exception) {
                //  code to handle error
            }
        });
    }

    private class BackgroundTask extends AsyncTask<String, Void, String> {
        Intent intent;

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            StringBuilder result = new StringBuilder();


//            URL url = new URL("http://192.168.1.3:8888/user.php");
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://www.zxcvb1234.net/lotto/user.php");

            try {
                // Add your data
//                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
//                nameValuePairs.add(new BasicNameValuePair("facebook_id", facebook_id));
//                nameValuePairs.add(new BasicNameValuePair("lotto_buy", textInput));
//                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//
//                // Execute HTTP Post Request
//                HttpResponse response = httpclient.execute(httppost);
//                Log.d("facebook_id",facebook_id);

            } catch (Exception e) {
                Log.e("error",e.toString());
                // TODO Auto-generated catch block
            }


            return result.toString();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();




        }



        @Override
        protected void onPostExecute(String result) {
            Intent intent = new Intent(MainActivity.this,MainActivity2.class);
            intent.putExtra("prize",prize);
            startActivity(intent);
        }
    }
}
