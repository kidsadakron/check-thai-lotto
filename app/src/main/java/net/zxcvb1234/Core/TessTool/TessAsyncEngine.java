package net.zxcvb1234.Core.TessTool;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import net.zxcvb1234.Core.Dialogs.ImageDialog;
import net.zxcvb1234.Core.Imaging.Tools;
import net.zxcvb1234.lottery.MainActivity2;


/**
 * Created by Fadi on 6/11/2014.
 */
public class TessAsyncEngine extends AsyncTask<Object, Void, String> {

    static final String TAG = "DBG_" + TessAsyncEngine.class.getName();

    private Bitmap bmp;

    private Activity context;


    @Override
    protected String doInBackground(Object... params) {

        try {

            if(params.length < 2) {
                Log.e(TAG, "Error passing parameter to execute - missing params");
                return null;
            }

            if(!(params[0] instanceof Activity) || !(params[1] instanceof Bitmap)) {
                Log.e(TAG, "Error passing parameter to execute(context, bitmap)");
                return null;
            }

            context = (Activity)params[0];

            bmp = (Bitmap)params[1];
            int[] pix = new int[bmp.getWidth() * bmp.getHeight()];
            int picw = bmp.getWidth();
            int pich = bmp.getHeight();
            bmp.getPixels(pix, 0, picw, 0, 0, picw, pich);

            int R, G, B,Y;

            for (int y = 0; y < pich; y++){
                for (int x = 0; x < picw; x++)
                {
                    int index = y * picw + x;
                    R = (pix[index] >> 16) & 0xff;     //bitwise shifting
                    G = (pix[index] >> 8) & 0xff;
                    B = pix[index] & 0xff;

                    //R,G.B - Red, Green, Blue
                    //to restore the values after RGB modification, use
                    //next statement
                    pix[index] = 0xff000000 | (R << 16);
                    bmp.setPixel(x,y,pix[index]);
//                Log.d(TAG, "Got bitmap" + pix[index]);
                }}

            if(context == null || bmp == null) {
                Log.e(TAG, "Error passed null parameter to execute(context, bitmap)");
                return null;
            }

            int rotate = 0;

            if(params.length == 3 && params[2]!= null && params[2] instanceof Integer){
                rotate = (Integer) params[2];
            }

            if(rotate >= -180 && rotate <= 180 && rotate != 0)
            {
                bmp = Tools.preRotateBitmap(bmp, rotate);
                Log.d(TAG, "Rotated OCR bitmap " + rotate + " degrees");
            }

            TessEngine tessEngine =  TessEngine.Generate(context);

            bmp = bmp.copy(Bitmap.Config.ARGB_8888, true);

            String result = tessEngine.detectText(bmp);

            Log.d(TAG, result);

            return result;

        } catch (Exception ex) {
            Log.d(TAG, "Error: " + ex + "\n" + ex.getMessage());
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {

        if(s == null || bmp == null || context == null)
            return;
        Log.d("text" , s);
      /*  if(s.isEmpty()){

        }else
        {
            Intent intent = new Intent(context , MainActivity2.class);
            intent.putExtra("text",s);
            context.startActivity(intent);
        }*/
        Intent intent = new Intent(context , MainActivity2.class);
        intent.putExtra("text", s);
        context.setResult(context.RESULT_OK,intent);
        context.finish();
//
//

       ImageDialog.New()
                .addBitmap(bmp)
                .addTitle(s)
                .show(context.getFragmentManager(), TAG);

        super.onPostExecute(s);
    }
}
